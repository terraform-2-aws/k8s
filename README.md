### Fluentbit
- The yaml fiels can be used to create fluentbit on EKS cluster on AWS provider.
- The fluentbit collects the k8s logs and send it to the AWS kinesis and finally the logs will be stored on a S3 bucket.
- an nginx image is also set into a yaml file to create logs by calling the EKS endpoint.
